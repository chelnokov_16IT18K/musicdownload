package ru.chelnokov.images.main;

import java.io.*;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Класс, реализующий скачивание первых пяти изображений из ленты сайта https://fishki.net
 * (https://fishki.net/tag/kartinki/)
 *
 * @author Chelnokov E.I. 16IT18k
 */
public class Main {

    private static final String IN_FILE_TXT = "src\\ru\\chelnokov\\images\\download\\inFile.txt";
    private static final String OUT_FILE_TXT = "src\\ru\\chelnokov\\images\\download\\outFile.txt";
    private static final String PATH_TO_PIC = "src\\ru\\chelnokov\\images\\download\\img";

    public static void main(String[] args) {
        String Url;
        try (BufferedReader inFile = new BufferedReader(new FileReader(IN_FILE_TXT));
             BufferedWriter outFile = new BufferedWriter(new FileWriter(OUT_FILE_TXT))) {
            while ((Url = inFile.readLine()) != null) {
                URL url = new URL(Url);

                String result;
                try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(url.openStream()))) {
                    result = bufferedReader.lines().collect(Collectors.joining("\n"));
                }
                Pattern email_pattern = Pattern.compile("\\s*(?<=img\\ssrc\\s?=\\s?\")([\\/\\w\\.-]*)*");
                Matcher matcher = email_pattern.matcher(result);
                writeFound(outFile, matcher);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        writeFiles();

    }

    /**
     * Записывает найденные по заданному шаблону ссылки на картинки в текстовый файл
     * @param outFile текстовый файл для записи
     * @param matcher объект, подходящий по регулярному выражению
     * @throws IOException ошибка данных
     */
    private static void writeFound(BufferedWriter outFile, Matcher matcher) throws IOException {
        int i = 0;
        while (matcher.find() && i < 5) {
            if (!matcher.group().contains("https:")) {
                outFile.write("https:" + matcher.group() + "\r\n");
                i++;
            }
        }
    }

    /**
     * Вызывает метод загрузки по считанным из файла outFile.txt ссылкам
     */
    private static void writeFiles() {
        try (BufferedReader imageFile = new BufferedReader(new FileReader(OUT_FILE_TXT))) {
            String image;
            int count = 0;
            try {
                while ((image = imageFile.readLine()) != null) {
                    downloadUsingNIO(image, PATH_TO_PIC + String.valueOf(count) + ".jpg");
                    count++;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @param strUrl ссылка на картинку
     * @param file полное имя файла
     * @throws IOException ну что-то там, извините
     */
    private static void downloadUsingNIO(String strUrl, String file) throws IOException {
        URL url = new URL(strUrl);
        ReadableByteChannel byteChannel = Channels.newChannel(url.openStream());
        FileOutputStream stream = new FileOutputStream(file);
        stream.getChannel().transferFrom(byteChannel, 0, Long.MAX_VALUE);
        stream.close();
        byteChannel.close();
    }
}
