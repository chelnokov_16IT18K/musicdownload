package ru.chelnokov.downloadmusic.main;

import java.io.*;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Main {

    private static final String IN_FILE_TXT = "src\\ru\\chelnokov\\downloadmusic\\download\\inFile.txt";
    private static final String OUT_FILE_TXT = "src\\ru\\chelnokov\\downloadmusic\\download\\outFile.txt";
    private static final String PATH_TO_MUSIC = "src\\ru\\chelnokov\\downloadmusic\\download\\music";

    public static void main(String[] args) {
        String Url;
        try (BufferedReader inFile = new BufferedReader(new FileReader(IN_FILE_TXT));
             BufferedWriter outFile = new BufferedWriter(new FileWriter(OUT_FILE_TXT))) {
            while ((Url = inFile.readLine()) != null) {
                URL url = new URL(Url);

                String result;
                try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(url.openStream()))) {
                    result = bufferedReader.lines().collect(Collectors.joining("\n"));
                }
                Pattern email_pattern = Pattern.compile("\\s*(?<=data-url\\s?=\\s?\")[^>]*\\/*(?=\")");
                Matcher matcher = email_pattern.matcher(result);
                writeFinded(outFile, matcher);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        writeFiles();

    }

    /**
     * находит 2 файла, соответствующие данному регулярному выражению и записывает их ссылки в файл outFile.txt
     * @param outFile текстовый файл
     * @param matcher
     * @throws IOException
     */
    private static void writeFinded(BufferedWriter outFile, Matcher matcher) throws IOException {
        int i = 0;
        while (matcher.find() && i < 2) {
            outFile.write(matcher.group() + "\r\n");
            i++;
        }
    }

    /**
     * метод для статистики файлов, поставленных на загрузку
     */
    private static void writeFiles() {
        try (BufferedReader musicFile = new BufferedReader(new FileReader(OUT_FILE_TXT))) {
            String music;
            int count = 0;
            try {
                while ((music = musicFile.readLine()) != null) {
                    downloadUsingNIO(music, PATH_TO_MUSIC + String.valueOf(count) + ".mp3");
                    count++;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * метод загрузки музыки
     * @param strUrl
     * @param file
     * @throws IOException
     */
    private static void downloadUsingNIO(String strUrl, String file) throws IOException {
        URL url = new URL(strUrl);
        ReadableByteChannel byteChannel = Channels.newChannel(url.openStream());
        FileOutputStream stream = new FileOutputStream(file);
        stream.getChannel().transferFrom(byteChannel, 0, Long.MAX_VALUE);
        stream.close();
        byteChannel.close();
    }
}
