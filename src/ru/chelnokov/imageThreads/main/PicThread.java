package ru.chelnokov.imageThreads.main;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

/**
 * Класс потока для загрузки картинки
 *
 * @author Chelnokov E.I. 16IT18k
 */
public class PicThread extends Thread {

    private static final String PATH_TO_PIC = "src\\ru\\chelnokov\\imageThreads\\download\\img";//путь загрузки
    private static String outFile;//ссылка на файл

    /**
     * конструктор
     * @param outFile ссылка на файл
     */
    PicThread(String outFile) {
        PicThread.outFile = outFile;
    }

    /**
     * метод для запуска скачивания
     */
    @Override
    public void run() {
        try {
            System.out.println("Поток " + getName() + " начал скачивание");
            downloadUsingNIO(outFile, PATH_TO_PIC + String.valueOf(getId()) + ".jpg");
            System.out.println("Файл " + PATH_TO_PIC + String.valueOf(getId()) + ".jpg" + " скачан успешно");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * скачивает картинку
     *
     * @param strUrl ссылка на картинку
     * @param file   полное имя файла
     * @throws IOException ну что-то там, извините
     */
    private static void downloadUsingNIO(String strUrl, String file) throws IOException {
        URL url = new URL(strUrl);
        ReadableByteChannel byteChannel = Channels.newChannel(url.openStream());
        FileOutputStream stream = new FileOutputStream(file);
        stream.getChannel().transferFrom(byteChannel, 0, Long.MAX_VALUE);
        stream.close();
        byteChannel.close();
    }

    String getOutFile() {
        return outFile;
    }

}
