package ru.chelnokov.imageThreads.main;

import java.io.*;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Класс, реализующий ПАРАЛЛЕЛЬНОЕ скачивание изображений из ленты сайта https://fishki.net
 * (https://fishki.net/tag/kartinki/)
 *
 * @author Chelnokov E.I. 16IT18k
 */
public class Main {

    private static final String IN_FILE_TXT = "src\\ru\\chelnokov\\imageThreads\\download\\inFile.txt";

    public static void main(String[] args) {
        String Url;//исходная url- строка
        PicThread[] load = new PicThread[6];
        try (BufferedReader inFile = new BufferedReader(new FileReader(IN_FILE_TXT))) {
            while ((Url = inFile.readLine()) != null) {

                URL url = new URL(Url);

                String result = readHtml(url);
                Pattern email_pattern = Pattern.compile("\\s*(?<=img\\ssrc\\s?=\\s?\")([\\/\\w\\.-]*)*");
                Matcher matcher = email_pattern.matcher(result);

                //writeFound
                int i = 0;

                while (matcher.find() && i < 6) {
                    if (!matcher.group().contains("https")) {
                        load[i] = new PicThread("https:" + matcher.group() + "\r\n");
                        load[i].start();
                        i++;
                    }
                }

            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Возвращает считанный код HTML-страницы
     * @param url адрес страницы, код которой будет считан
     * @return result считанный HTML-код
     * @throws IOException ошибка, связанная с URL
     */
    private static String readHtml(URL url) throws IOException {
        String result;
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(url.openStream()))) {
            result = bufferedReader.lines().collect(Collectors.joining("\n"));
        }
        return result;
    }


}
